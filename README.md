# LP GPT

Vue 3 + Vite

### 使用tailwindcss

> 安装 

```sh
pnpm i tailwindcss autoprefixer -D 
npx tailwindcss init
```



> 根目录下创建postcss.config.cjs

```js
module.exports = {
  plugins: {
    tailwindcss: {},
    autoprefixer: {},
  },
}
```

> src/main.js

```js
import './assets/tailwind.css'
```

> src/assets/tailwind.css

```css
@tailwind base;
@tailwind components;
@tailwind utilities;
```
