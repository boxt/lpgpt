import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import unpluginAutoImport from 'unplugin-auto-import/vite'

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  return {
    base: './',
    resolve: {
      alias: [{
        find: '@',
        replacement: resolve(__dirname, 'src')
      }]
    },
    server: {
      port: 3008,
      host: true,
      open: true,
      proxy: {
        '/testai': {
          target: 'https://www.icu007.top/testai',
          changeOrigin: true,
          rewrite: path => {
            return path.replace(/^\/testai/, '')
          }
        }
      }
    },
    esbuild: {
      drop: command === 'build' ? ['console', 'debugger'] : [],
    },
    build: {
      target: 'es2015',
      cssTarget: ['chrome52'],
      cssCodeSplit: true,
      emptyOutDir: true,
      // 启用/禁用 压缩大小报告。压缩大型输出文件可能会很慢，因此禁用该功能可能会提高大型项目的构建性能。
      reportCompressedSize: false,
      // chunk 大小警告的限制（以 kbs 为单位）默认： 500
      chunkSizeWarningLimit: 800,
      rollupOptions: {
        output: {
          manualChunks: {
            vue: ['vue'],
          }
        }
      }
    },
    plugins: [
      vue({
        // 响应性语法糖目前默认是关闭状态，需要你显式选择启用
        reactivityTransform: true
      }),
      unpluginAutoImport({
        imports: ['vue', 'vue-router', 'pinia'],
        dts: false
      })
    ],
  }
})
