import { createI18n } from 'vue-i18n'

import zh from './zh'
import en from './en'

const lang = (navigator.language || 'en').toLocaleLowerCase()

const i18n = createI18n({
  legacy: false,
  // locale: localStorage.getItem('lang') || lang.split('-')[0] || 'en',
  locale: localStorage.getItem('lang') || 'en',
  fallbackLocale: 'en', // 备用语言
  messages: {
    zh,
    en
  }
})

export default i18n