export default {
  loadingText: '处理中...',
  stopResponding: '停止',
  translationBy: '翻译提供方 chatGPT',
  sideBar: {
    newChat: '新会话',
    username: '游客',
    clearChats: '清空会话',
    refresh: '刷新页面',
    language: '语言',
    logout: '登出',
  },
  sendForm: {
    placeholder: '发送消息...',
  },
  aitype: { // key要和 src/lib/hooks/useAiType中的一致
    'knowledgebase': 'LM知识库',
    'wzmeihua': '内容改写',
    'summarize': '内容总结',
    'findImg': '发现好图',
    'fanyi': '翻译(X→英）',
  },
  welcome: {
    title: '西浦"君谋"AI平台使用规范',
    desc: `
    西浦"君谋"AI平台为教学与科研提供有力支持。但需注意，在此平台上所有聊天记录皆会被后台记录并妥善保管。
    ---
    西浦"君谋"AI平台是基于微软Azure OpenAI模型搭建的，响应速度受微软服务器地点及算力的影响，平均交互时间为5-10秒。
    ---
    确保问题准确：在提问之前，请确保自己已经仔细思考过这个问题，将问题描述清楚。
    ---
    不得侵犯他人权益：在提问时，请不要涉及到他人隐私、商业机密等敏感信息，不得违反国家法律法规。
    ---
    用语规范：在提问时，请使用正常的书面语句表达，尽量不要使用缩写、俚语或者网络用语等不规范的表达方式。---不涉及敏感话题：不得在提问中涉及到政治、宗教、种族等敏感话题，以免引起不必要的争议和误会。
    ---
    合理利用资源：西浦"君谋"AI平台的资源是有限的，因此请合理利用西浦"君谋"AI平台的服务，不要恶意刷屏、占用资源等行为。
    `,

    examples: {
      title: '示例',
      desc1: '用简单的术语解释量子计算',
      desc2: '对一个10岁的生日有什么创意?',
      desc3: '如何在 JavaScript 中发出 HTTP 请求?',
    },
    capabilities: {
      title: '能力',
      desc1: '记住用户之前在对话中所说的话',
      desc2: '允许用户提供后续更正',
      desc3: '接受过拒绝不当请求的培训',
    },
    limitations: {
      title: '局限性',
      desc1: '可能偶尔生成不正确的信息',
      desc2: '可能偶尔产生有害说明或有偏见的内容',
      desc3: '对2021年后的世界和事件了解有限',
    },
  }
}