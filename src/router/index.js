import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
import useUserStore from '@/stores/user'


const routes = [
  { path: '/', name: 'root', redirect: '/index' },
  { path: '/index', name: 'index', component: () => import('@/pages/index/index.vue') },
  { path: '/login', name: 'login', component: () => import('@/pages/login/login.vue') },
  { path: '/500', name: '500', component: () => import('@/components/500.vue') },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

// 开启登录检测
if (import.meta.env.DEV) {

} else {
  router.beforeEach((to, from, next) => {
    const { user } = useUserStore()
    if (!user && to.name !== 'login') {
      return next('/login')
    }
    next()
  })
}



export default router