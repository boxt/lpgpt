import { createApp } from 'vue'
import pinia from './stores/index.js'
import i18n from './lang/index.js'
import './assets/tailwind.css'
import './assets/index.css'

import router from './router'
import App from './App.vue'

const app = createApp(App)

app
  .use(pinia)
  .use(router)
  .use(i18n)
  .mount('#app')
