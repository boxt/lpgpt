import hljs from 'highlight.js/lib/core'
import javascript from 'highlight.js/lib/languages/javascript'
// import 'highlight.js/styles/github.css'
// import 'highlight.js/styles/github-dark.css'
// import 'highlight.js/styles/gradient-dark.css'
import 'highlight.js/styles/monokai-sublime.css'



hljs.registerLanguage('javascript', javascript);

export default hljs