export function getQuery(queryString = '') {
  let str = queryString || location.search;
  let objURL = {};

  str.replace(
    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
    function ($0, $1, $2, $3) {
      objURL[$1] = decodeURIComponent($3)
    }
  )
  return objURL
}