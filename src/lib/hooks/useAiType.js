
export default function useHook(emit) {

  const aitypes = ref([
    { type: 'lmknbase', name: 'LM知识库', en: 'Knowledge Base' },
    { type: 'wzmeihua', name: '内容改写', en: 'Content Polish' },
    { type: 'summarize', name: '内容总结', en: 'Content Summary' },
    // { type: 'findImg', name: '发现好图', en: 'Image discovered' },
    { type: 'fanyi', name: '翻译(X→英）', en: 'Translate To English' },
    { type: 'genImges', name: '图像生成', en: 'Generate Images' },
  ])

  return {
    aitypes
  }
}