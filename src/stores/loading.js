import { defineStore } from "pinia"

const useStore = defineStore({
  id: "loading",
  state: () => ({
    talking: false,
    abortCtrl: null
  }),
  actions: {
    setAbortCtrl(ctrl) {
      this.abortCtrl = ctrl
      // 监听abort
      ctrl.signal.addEventListener('abort', () => {
        this.close()
      })
    },
    stop() {
      if (!this.abortCtrl) {
        return this.close()
      }
      this.abortCtrl.abort()
    },
    toggle() {
      this.talking = !this.talking
    },
    open() {
      this.talking = true
    },
    close() {
      this.talking = false
    }
  }
})

export default useStore