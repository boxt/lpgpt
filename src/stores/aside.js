import { defineStore } from "pinia"

const useStore = defineStore({
  id: "aside",
  state: () => ({
    status: false
  }),
  actions: {
    toggle() {
      this.status = !this.status
    },
    open() {
      this.status = true
    },
    close() {
      this.status = false
    }
  }
})

export default useStore