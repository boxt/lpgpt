// import request from '@/lib/js/request.js'
// const r = request({ headers: { 'content-type': 'application/json' } })

import r from '@/lib/js/fetch.js'
const api = import.meta.env.DEV ? {
  chatgpt: data => r.steam('/testai/chatgpt.php', data), // 会话
  streamai: data => r.steam('/testai/streamai.php', data), // 获取steam流
  getuserinfo: data => r.steam('/testai/getuserinfo.php', data), // 获取用户信息
  loginOut: data => r.steam('/testai/logout.php', data), // 退出
} : {
  chatgpt: data => r.steam('/api/chatgpt', data), // 会话 暂时废弃
  streamai: data => r.steam('/api/chatstream', data), // 获取steam流
  getuserinfo: data => r.steam('/api/chatuser/getuserinfo', data), // 获取用户信息
  loginOut: data => r.steam('/api/chatuser/loginOut', data), // 退出
}

export default api
